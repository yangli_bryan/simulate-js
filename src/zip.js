// TODO: Write a function `zip`. Please review the code in
// the unit test to understand its usage.
// <-start-
function zip (left, right) {
  const result = [];
  if (left.length === 0 || right.length === 0) {
    return result;
  } else {
    const smallerLength = Math.min(left.length, right.length);
    for (let i = 0; i < smallerLength; i++) {
      result.push([left[i], right[i]]);
    }
    return result;
  }
}
// --end->

export default zip;
