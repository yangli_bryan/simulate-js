// TODO: Write a class `Receptionist`. Please review the code
// in the unit test to understand its usage.
// <-start-
class Receptionist {
  constructor () {
    this.list = [];
    this.idList = [];
  }

  register (item) {
    if (item === null || item === undefined || item === '' || item === {} ||
            {}.hasOwnProperty.call(item, 'id') === false || {}.hasOwnProperty.call(item, 'name') === false) {
      throw new Error('Invalid person');
    } else if (this.idList.includes(item.id) === true) {
      throw new Error('Person already exist');
    } else {
      this.list.push(item);
      this.idList.push(item.id);
    }
    return this.list;
  }

  getPerson (index) {
    if (this.idList.includes(index) === true) {
      const newIndex = this.idList.findIndex((currentId) => currentId === index);
      return this.list[newIndex];
    } else {
      throw new Error('Person not found');
    }
  }
}
// --end-->

export default Receptionist;
